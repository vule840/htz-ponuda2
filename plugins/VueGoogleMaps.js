import Vue from "vue"
import * as VueGoogleMaps from "~/node_modules/vue2-google-maps"

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyC-kvcJL-wQ6CN458FQG2eLCALXEp0UVb4",
    libraries: "visualization",
  },
  installComponents: true,
})
